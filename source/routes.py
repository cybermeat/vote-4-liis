from source import app, db
from config import readme
from source.models import *
from flask import request, jsonify, Response
from functools import wraps
from werkzeug.security import generate_password_hash, check_password_hash
import markdown2
import re

API_V1_RES = '/api/v1'


def shield_name(str_obj):
	if type(str_obj) == str:
		return re.sub(r'[^\w]', '', str_obj)
	return ''


def shield_num(str_obj):
	if type(str_obj) == str:
		return re.sub(r'[^\d]', '', str_obj)
	return str_obj


def poll_to_dict(poll, user):
	options = []
	votes_quantity = poll.votes_qty()
	for option in poll.options:
		voters_id = [voter.user_id for voter in option.members]
		options.append({
			"option_id": option.id,
			"option_text": option.answer,
			"voters_id": voters_id,
			"popularity": "{:.2f}%".format(option.members_qty() / votes_quantity * 100 if votes_quantity > 0 else 0),
			"selected": True if user and user.id in voters_id else False
		})
	return {"question": poll.question, "votes_quantity": votes_quantity, "options": options}


def auth_prompt():
	return Response(
		'You need to login for this action', 401,
		{'WWW-Authenticate': 'Basic realm="Login Required"'}
	)


def auth_required(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		auth = request.authorization
		if auth:
			user = User.query.filter_by(username=auth.username).first()
			if user and check_password_hash(user.password_hash, auth.password):
				return f(*args, **kwargs)
		return auth_prompt()

	return wrap


@app.route('/', methods=['GET'])
def get_readme():
	if readme:
		print(readme)
		return markdown2.markdown_path(readme)
	return page_not_found()


@app.route(API_V1_RES + '/users', methods=['GET'])
def api_get_user():
	qu_params = request.args
	username = qu_params.get('username')
	if not username:
		users = User.query.all()
		result = []
		for u in users:
			result.append({"id": u.id, "username": u.username})
		return jsonify(result)
	user = User.query.filter_by(username=shield_name(username)).first()
	if not user:
		return page_not_found()
	return jsonify({"id": user.id, "username": user.username})


@app.route(API_V1_RES + '/users', methods=['POST'])
def api_create_user():
	request_json = request.get_json(force=True)
	if 'username' not in request_json or 'password' not in request_json:
		return bad_request(request_json)
	username = shield_name(request_json['username'])
	check = User.query.filter_by(username=username).first()
	if check:
		return jsonify({"status": {
			'type': 'error',
			'message': "Username [{}] already exists".format(request_json['username'])}}), 409
	new_user = User(username=username, password_hash=generate_password_hash(request_json['password']))
	db.session.add(new_user)
	db.session.commit()
	return jsonify({'status': {'type': 'success', 'message': "New user [{}] created".format(new_user.username)}}), 201


@app.route(API_V1_RES + '/users', methods=['DELETE'])
def api_remove_user():
	qu_params = request.args
	username = shield_name(qu_params.get('username'))
	if not username:
		return bad_request(qu_params)
	user = User.query.filter_by(username=username).first()
	if not user:
		return page_not_found()
	db.session.delete(user)
	db.session.commit()
	return jsonify({'status': {'type': 'success', 'message': "User [{}] removed".format(username)}})


@app.route(API_V1_RES + '/polls', methods=['GET'])
def api_get_poll():
	if request.authorization:
		user = User.query.filter_by(username=request.authorization.username).first()
	else:
		user = None
	qu_params = request.args
	if not qu_params.get('id'):
		polls = Poll.query.all()
		response = {}
		for poll in polls:
			response[poll.id] = poll_to_dict(poll, user)
		return jsonify(response)
	poll = Poll.query.filter_by(id=shield_num(qu_params.get('id'))).first()
	if not poll:
		return page_not_found()
	return jsonify(poll_to_dict(poll, user))


@app.route(API_V1_RES + '/polls', methods=["POST"])
def api_create_poll():
	request_json = request.get_json(force=True)
	if "question" not in request_json:
		return bad_request(request_json)
	poll = Poll(question=request_json['question'])
	db.session.add(poll)
	db.session.commit()
	if "options" in request_json:
		for option in request_json["options"]:
			if "option_text" in option:
				vote_opt = VoteOption(answer=option["option_text"], poll_id=poll.id)
				db.session.add(vote_opt)
	db.session.commit()
	return jsonify({'status': {'type': 'success', 'message': 'Poll ID#{} created'.format(poll.id)}}), 201


@app.route(API_V1_RES + '/polls', methods=["PUT"])
def api_update_poll():
	request_json = request.get_json(force=True)
	if "id" not in request_json:
		return bad_request(request_json)
	poll = Poll.query.filter_by(id=shield_num(request_json['id'])).first()
	if not poll:
		return page_not_found()
	if "question" in request_json:
		poll.question = request_json['question']
	if "options" in request_json:
		for option in request_json["options"]:
			if "option_text" in option:
				if "option_id" in option:
					opt = VoteOption.query.filter_by(id=shield_num(option["option_id"]), poll_id=poll.id).first()
					if opt:
						opt.answer = option["option_text"]
				else:
					opt = VoteOption(answer=option["option_text"], poll_id=poll.id)
					db.session.add(opt)
	db.session.commit()
	return jsonify({'status': {'type': 'success', 'message': 'Poll ID#{} edited'}})


@app.route(API_V1_RES + '/polls', methods=['DELETE'])
def api_remove_poll():
	qu_params = request.args
	poll_id = shield_num(qu_params.get('id'))
	if not poll_id:
		return bad_request(qu_params)
	poll = Poll.query.filter_by(id=poll_id).first()
	if not poll:
		return page_not_found()
	db.session.delete(poll)
	db.session.commit()
	return jsonify({'status': {'type': 'success', 'message': "Poll ID#{} removed".format(poll_id)}})


@app.route(API_V1_RES + '/vote', methods=['POST'])
@auth_required
def api_vote():
	request_json = request.get_json(force=True)
	if "poll_id" not in request_json or "opt_id" not in request_json:
		return bad_request(request_json)
	user = User.query.filter_by(username=request.authorization.username).first()
	poll = Poll.query.filter_by(id=shield_num(request_json["poll_id"])).first()
	if not poll:
		return page_not_found()
	option = VoteOption.query.filter_by(id=shield_num(request_json["opt_id"])).first()
	if not option:
		return page_not_found()
	if option.poll_id != poll.id:
		return bad_request(request_json)
	voted = UserVote.query.filter_by(user_id=user.id, poll_id=poll.id).first()
	if voted:
		db.session.delete(voted)
	new_vote = UserVote(user_id=user.id, vote_id=option.id, poll_id=poll.id)
	db.session.add(new_vote)
	db.session.commit()
	return jsonify({'status': {
		'type': 'success',
		'message': "Vote from user [{}] received".format(user.username)}
	}), 202


def page_not_found():
	return jsonify({'status': {'type': 'error', 'message': 'The resource could not be found'}}), 404


def bad_request(request_data):
	return jsonify({'status': {
		'type': 'error',
		'message': 'Invalid request. Please, check received parameters: {}'.format(request_data)}
	}), 400
