import requests
import unittest
import random
import os
from config import basedir
from source import app, db
from source.models import *
from source.routes import API_V1_RES

DB_PATH = os.path.join(basedir, 'database.db')


class TestCase(unittest.TestCase):
	def setUp(self):
		self.base_url = 'http://127.0.0.1:5000' + API_V1_RES
		self.users_cred = []
		db.drop_all()
		db.create_all()

	def test_populate_db(self):
		print("[.]Creating users...")
		for i in range(7):
			username = 'user' + str(i)
			password = 'pswd'
			response = requests.post(self.base_url + '/users', json={'username': username, 'password': password})
			assert response.status_code == 201
			self.users_cred.append((username, password))

		print("[.]Creating polls...")
		template = {
			'question': '',
			'options': [
				{
					'option_text': "Ответ#1"
				},
				{
					'option_text': "Ответ#2"
				}
			]
		}
		for i in range(3):
			template["question"] = 'Опрос#{}'.format(i)
			response = requests.post(self.base_url + '/polls', json=template)
			assert response.status_code == 201

		print("[.]Editing polls...")
		template1 = {
			'id': 1,
			'options': [
				{
					'option_text': "Новый вариант ответа"
				}
			]
		}
		template2 = {
			'id': 2,
			'question': 'Отредактированный вопрос'
		}
		response = requests.put(self.base_url + '/polls', json=template1)
		assert response.status_code == 200
		response = requests.put(self.base_url + '/polls', json=template2)
		assert response.status_code == 200

		print("[.]Creating votes...")
		response = requests.get(self.base_url + '/polls', auth=self.users_cred[0])
		assert response.status_code == 200
		respdata = response.json().copy()
		for pkey in respdata:
			for user in self.users_cred:
				opt_ids = [opt["option_id"] for opt in respdata[pkey]["options"]]
				response = requests.post(
					self.base_url + '/vote',
					json={'poll_id': pkey, 'opt_id': random.choice(opt_ids)},
					auth=user)
				assert response.status_code == 202


if __name__ == '__main__':
	unittest.main()
