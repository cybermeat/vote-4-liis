from source import db


class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	password_hash = db.Column(db.String(128))
	
	def __repr__(self):
		return '[{}]'.format(self.username)


class Poll(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	question = db.Column(db.String(255))
	options = db.relationship('VoteOption', backref='poll', lazy='dynamic')
	
	def votes_qty(self):
		return UserVote.query.filter_by(poll_id=self.id).count()


class VoteOption(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	answer = db.Column(db.String(255))
	poll_id = db.Column(db.Integer, db.ForeignKey('poll.id'))
	members = db.relationship('UserVote', backref='option', lazy='dynamic')
	
	def members_qty(self):
		return UserVote.query.filter_by(vote_id=self.id).count()


class UserVote(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	vote_id = db.Column(db.Integer, db.ForeignKey('vote_option.id'))
	poll_id = db.Column(db.Integer, db.ForeignKey('poll.id'))
